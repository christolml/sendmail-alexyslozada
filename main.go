package main

import (
	"bytes" //nos permite ir escribiendo informacion que ir concatenando string
	"fmt"
	"log" //por si hay un error durante la ejecucion del proceso

	// "net"		//crea las conexiones
	"crypto/tls"    //permite configurar las opciones tls de seguridad de gmail
	"html/template" //toma el archivo html que se creo como template.htlm toma la informacion y enviarla como correo
	"net/mail"      //permite traer la estructura de un correo electr, el nombre y la direccion del correo elect
	"net/smtp"      //permite enviar los correos
)

// esta es una estructura pensada para el name que debemos enviar al template.HTML
type Dest struct {
	Name string
}

func checkErr(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func main() {
	from := mail.Address{"Christopher Veláz", "cvelazquez@ucol.mx"}
	to := mail.Address{"ChristoPro Envio", "christo_9722@hotmail.com"}
	subject := "Correo enviado desde GO"

	dest := Dest{Name: to.Address} //se crea la estancia de Dest, para declarar el name

	/* se configuran los headers del correo
	la informacion que tenemos en las variables de arriba las colocamos en un mapa de string */
	headers := make(map[string]string)
	// el metodo String lo que hace es devolver el correo electronico de la structura Address
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subject
	// en el content-type le decimos que es html, ya que si no le decimos nuestro template.HTML aparece tal cual y como esta con sus etiquetas y todo
	headers["Content-Type"] = `text/html; charset="UTF-8"`

	/*
		Sprintf formatea de acuerdo con un especificador de formato y devuelve la cadena resultante.
	*/

	// ahora se necesita armar el correo que se va a enviar, este message tiene el encabezado
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	/*
		ParseFiles crea una nueva plantilla y analiza las definiciones de la plantilla de los archivos nombrados.
		El nombre de la plantilla devuelta tendrá el nombre (base) y el contenido (parsed) del primer archivo.
		Debe haber al menos un archivo. Si se produce un error, el análisis se detiene y devuelve *Template is nil

		Al analizar múltiples archivos con el mismo nombre en diferentes directorios, el último que se menciona será el que resulte.
		Por ejemplo, ParseFiles ("a / foo", "b / foo") almacena "b / foo" como la plantilla llamada "foo", mientras que "a / foo" no está disponible.  */

	// se toma el template y pasarle la informacion  que se encuentra en la estructura de dest := Dest{Name: to.Address}
	template, err := template.ParseFiles("template.html")
	// si hay un error no lo muestra
	checkErr(err)

	// el buf es el contenido del template ya con el destinatario
	// se ejecutará el template y guardalrlo en un slice de bite
	buf := new(bytes.Buffer)
	// ejecutamos el template, le enviamos como salida el buffer, ees decir cuando haga el parse lo pase al buffer y el dato que necesita es el destinatario
	err = template.Execute(buf, dest)
	checkErr(err)

	// aqui se tiene todo el mensaje listo para enviar, el mensaje es la combinación de message(los headers con la informacion a enviar) y de buf(contenido template)
	message += buf.String()

	// ahora lo que se hará es conectarnos a nuestro correo electronico para enviar el mensaje

	servername := "smtp.gmail.com:465"
	host := "smtp.gmail.com" //el host que va usar para la autentificacion

	// se obtiene la autenticación
	auth := smtp.PlainAuth("", "cvelazquez@ucol.mx", "password-correo-aqui-poner", host)

	// se configura el tls para que se salte la autenticacion y no tener problemas con firmas y cosas así
	tlsConfig := &tls.Config{ //hace referencia a un puntero de tls.Config
		InsecureSkipVerify: true, //se salta la verificación insegura
		ServerName:         host,
	}

	// ahora se hace la conexión al servidor de gmail
	// se pasa el protocolo, el servidor con puerto y la configuracion tls que se creo arribita
	conn, err := tls.Dial("tcp", servername, tlsConfig)
	checkErr(err)

	// se crea el cliente de la conexión
	client, err := smtp.NewClient(conn, host)
	checkErr(err)

	// ahora a el cliente intentamos autenticarlo con la autenticación que esta arriba (auth)
	err = client.Auth(auth)
	checkErr(err)

	// se sigue diciendole al cliente quien va hacer el que envia el correo y quien lo va a recibir
	err = client.Mail(from.Address)
	checkErr(err)

	err = client.Rcpt(to.Address)
	checkErr(err)

	// ahora hay que decirle cual es el data del correo, para ello obtenemos un writer
	w, err := client.Data()
	checkErr(err)

	// ahora vamos a escribir el mensaje que tenemos , el mensaje lo es enviado como un slice de bytes y entonces es por eso que hace un casting a message
	_, err = w.Write([]byte(message))
	checkErr(err)

	// se cierra la conexión si no ha ocurrido errores
	err = w.Close()
	checkErr(err)

	// nos salimos del cliente
	client.Quit()

}
